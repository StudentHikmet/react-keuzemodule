import axios from "axios";

export default axios.create({
    baseURL:'https://api.unsplash.com/search/photos',
    headers: {
        Authorization: 'Client-ID tXnt4Jd6DS3suHFEou-xEJXqNdlKsDzsmEqV1RKgKgQ'
    }
});